{-# LANGUAGE OverloadedStrings #-}
module Html (root) where

import           Prelude                     hiding (head)
import           Text.Blaze.Html5            as Html
import           Text.Blaze.Html5.Attributes as Attr hiding (title)

root :: Html
root = docTypeHtml $ do
  head $ do
    title "Petráč"
  body $ do
    p "Petráč" ! class_ "foo"
