{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Binary.Builder           as Builder
import           Data.Text                     (Text)
import qualified Data.Text                     as Text
import qualified Data.Text.Lazy.Encoding       as Lazy
import           Html
import qualified Network.HTTP.Types            as Http
import qualified Network.Wai                   as Wai
import qualified Network.Wai.Handler.Warp      as Warp
import           System.FilePath               ((</>))
import qualified Text.Blaze.Html.Renderer.Text as Blaze
import           Text.Blaze.Html5              (Html)

main :: IO ()
main = Warp.run 3000 app

app :: Wai.Application
app req respond =
  respond $
    case Wai.pathInfo req of
      ["assets", file] -> assets file
      path             -> html path

assets :: Text -> Wai.Response
assets file = Wai.responseFile Http.status200 [] ("assets" </> Text.unpack file) Nothing

html :: [Text] -> Wai.Response
html = \case
  []    -> render Html.root
  _path -> Wai.responseBuilder Http.status404 [] Builder.empty
  where
    render :: Html -> Wai.Response
    render = Wai.responseBuilder Http.status200 headers . Builder.fromLazyByteString . Lazy.encodeUtf8 . Blaze.renderHtml

    headers :: Http.ResponseHeaders
    headers = [(Http.hContentType, "text/html")]
